"use strict";

/*
 * Copyright (C) 2020-2021 UBports Foundation <info@ubports.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { promisify } from "util";
import checksum from "checksum";
import Axios from "axios";
import crypto from "crypto";
import fs from "fs-extra";

const calculateFileChecksum = promisify(checksum.file);

/**
 * @typedef ChecksumOptions
 *
 * @property {String} sum - universal resource locator
 * @property {String} algorithm - hashing algorithm
 */

/**
 * @typedef FileOptions
 *
 * @property {String} url - universal resource locator
 * @property {String} path - path of the downloaded file
 * @property {ChecksumOptions} checksum - checksum options
 */

/**
 * Validate a file based on the checksum
 *
 * @param {FileOptions} param0 - file options
 * @param {Boolean} ifNoChecksum - value to resolve if no checksum exists
 *
 * @returns {Promise} resolves true if the file exists and matches the provided hash, false otherwise
 */
export function checkFile({ path, checksum }, ifNoChecksum = false) {
  return checksum && crypto.getHashes().includes(checksum.algorithm)
    ? fs
        .pathExists(path)
        .then(exists =>
          exists
            ? calculateFileChecksum(path, checksum).then(
                calculatedSum => calculatedSum === checksum.sum
              )
            : false
        )
        .catch(error => {
          throw new Error("Checksum Error", error);
        })
    : Promise.resolve(ifNoChecksum);
}

/**
 * Download a file
 *
 * @param {String} url - universal resource locator
 * @param {String} downloadPath - path to download the file to
 * @param {Function} [chunkDownloaded] - progress callback function, called every time a chunk has been downloaded
 * @param {Function} [size] - total expected size
 */
export async function downloadOne(
  url,
  downloadPath,
  chunkDownloaded = () => {},
  size = () => {}
) {
  try {
    fs.ensureFileSync(downloadPath);
    const writer = fs.createWriteStream(downloadPath);
    const response = await Axios({
      url,
      type: "GET",
      responseType: "stream",
      headers: {
        Accept: "*/*"
      }
    });
    response.data.pipe(writer);
    size(eval(response.headers["content-length"]));
    response.data.on("data", data => {
      chunkDownloaded(data.length);
    });
    return new Promise((resolve, reject) => {
      writer.on("finish", resolve);
      writer.on("error", reject);
    });
  } catch (error) {
    throw new Error(`Download Error: ${url} ${error}`);
  }
}

/**
 *
 * @param {Array<FileOptions>} files - [{url, path, checksum: { sum, algorithm }}]
 * @param {Function} progress - called with a progress percentage
 * @param {Function} next - called with the number of completed downloads and the number of outstanding downloads
 * @param {Function} activity - called with "preparing" or "downloading"
 */
export function download(
  files,
  progress = () => {},
  next = () => {},
  activity = () => {}
) {
  return new Promise(function(resolve, reject) {
    let filesDownloaded = 0;
    let filesToDownload = 0;
    let overallSize = 0;
    let sizeDownloaded = 1;
    let lastSizeDownloaded = 0;
    let downloadProgress = 0;
    let progressInterval = null;
    const reportNext = (curr, total) => {
      next(curr, total);
      if (curr === total) {
        clearInterval(progressInterval);
      }
    };
    const reportProgress = chunkSize => {
      sizeDownloaded += chunkSize;
      if (!progressInterval) {
        progressInterval = setInterval(() => {
          downloadProgress = sizeDownloaded / overallSize;
          if (overallSize != 0) {
            if (downloadProgress < 0.999) {
              progress(
                downloadProgress,
                (sizeDownloaded - lastSizeDownloaded) / 1000000
              );
              lastSizeDownloaded = sizeDownloaded;
            } else if (filesDownloaded === filesToDownload) {
              clearInterval(progressInterval);
            }
          }
        }, 1000);
      }
    };
    activity("preparing");
    Promise.all(files.map(f => checkFile(f, false)))
      .then(results => files.filter((r, i) => !results[i]))
      .then(files =>
        files.length === 0
          ? Promise.resolve()
          : Promise.resolve(files).then(files => {
              filesToDownload = files.length;
              next(0, filesToDownload);
              activity("downloading");
              reportProgress(1);
              return Promise.all(
                files.map(file =>
                  downloadOne(
                    file.url,
                    file.path,
                    reportProgress,
                    s => (overallSize += s || 1)
                  )
                    .then(() => reportNext(++filesDownloaded, files.length))
                    .then(() => checkFile(file, true))
                    .then(ok =>
                      ok
                        ? Promise.resolve()
                        : Promise.reject(
                            new Error(`Checksum mismatch on file ${file.path}`)
                          )
                    )
                )
              );
            })
      )
      .then(() => resolve(files))
      .catch(error => {
        clearInterval(progressInterval);
        reject(new Error(error));
      });
  });
}
